package Ex5;

import javafx.event.ActionEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

public class UI extends JFrame {
    static int i = 1;
    JTextField station;
    JLabel s;
    JTextField destination;
    JLabel d;
    JTextField segment;
    JLabel se;
    JButton submit;
    ArrayList<Controler> lc;


    UI(ArrayList<Controler> list_controlers) {
        lc = list_controlers;
        setTitle("Simulator");
        init();
        setBounds(720, 0, 300, 300);
        setResizable(false);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 120;
        int height = 20;

        s = new JLabel("Station");
        s.setBounds(50, 50, width, height);

        station = new JTextField();
        station.setBounds(120, 50, width, height);


        d = new JLabel("Destination");
        d.setBounds(50, 80, width, height);
        destination = new JTextField();
        destination.setBounds(120, 80, width, height);

        se = new JLabel("Segment");
        se.setBounds(50, 110, width, height);
        segment = new JTextField();
        segment.setBounds(120, 110, width, height);

        submit = new JButton("Add");
        submit.setBounds(50, 140, width, height);
        submit.addActionListener(new SubmitData());

        Random rand = new Random();

        int redValue = rand.nextInt(255);
        int greenValue = rand.nextInt(255);
        int blueValue = rand.nextInt(255);

        Color color = new Color(redValue, greenValue, blueValue);

        submit.setBackground(color);
        add(s);
        add(d);
        add(se);
        add(station);
        add(destination);
        add(segment);
        add(submit);
    }


    class SubmitData implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //Take the value from the textBoxes
            String station_string = station.getText();
            String destination_string = destination.getText();
            String segment_string = segment.getText();


            boolean s = false;
            boolean d = false;
            boolean sg = false;
            for (Controler c : lc) {
                if (c.stationName.equals(station_string))
                    s = true;
                if (c.stationName.equals(destination_string))
                    d = true;

                if (c.getFreeSegmentId() != -1) {

                    if (Integer.parseInt(segment_string) >= 0 && Integer.parseInt(segment_string) <= 2)
                        sg = true;
                }

            }
            if (s && d) {
                if (sg) {

                    Train t1 = new Train(destination_string, "Add" + i);
                    i++;

                    for (Controler c : lc) {
                        if (c.stationName.equals(station_string)) {

                            c.list.get(Integer.valueOf(segment_string)).arriveTrain(t1);
                        }
                    }
                }
            }
        }

        @Override
        public void actionPerformed(java.awt.event.ActionEvent actionEvent) {

        }
    }
}

