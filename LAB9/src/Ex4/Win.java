package Ex4;

import javax.swing.*;
import java.awt.*;

public class Win extends JFrame {

    private boolean turn;
    private JButton[] buttons = new JButton[9];
    private int[] array = new int[10];


    public Win() {
        setTitle("Game");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(900, 900);
        setVisible(true);

        turn = false;
        for (int i = 0; i < 9; ++i) {
            buttons[i] = new JButton();
            buttons[i].setMinimumSize(new Dimension(100, 100));
            final int i2 = i;

            buttons[i].addActionListener(e -> {
                turn = !turn;
                if (turn == true) {
                    buttons[i2].setText("X");
                    array[i2] = 1;
                } else {
                    buttons[i2].setText("0");
                    array[i2] = 2;
                }
                check();

            });
            setLayout(new GridLayout(3, 3));
            add(buttons[i]);
            buttons[i].setVisible(true);
        }

    }

    public void check() {
       for(int i=1;i<=3;i++)
       {
           if((array[i] ==1 && array[i]==1 && array[i]==1)
               || (array[i+3] ==1 && array[i+3]==1 && array[i+3]==1)
               || (array[i+6] ==1 && array[i+6]==1 && array[i+6]==1)) {
                 System.out.println("X won");return ;}

           if(((array[i] ==2 && array[i]==2) && array[i]==2)
                   || (array[i+3] ==2 && array[i+3]==2 && array[i+3]==2)
                   || (array[i+6] ==2 && array[i+6]==2 && array[i+6]==2)) {
                 System.out.println("X won");return ;}

       }


        if(array[1] ==1 && array[5]==1 && array[9]==1) {  System.out.println("X won");return ;}
        if(array[3] ==1 && array[5]==1 && array[7]==1) {  System.out.println("X won");return ;}

        if(array[1] ==2 && array[5]==2 && array[9]==2) {  System.out.println("X won");return ;}
        if(array[1] ==2 && array[5]==2 && array[9]==2) {  System.out.println("X won");return ;}

    }

    public static void main(String[] args) {
        new Win();
    }
}
