package Ex4;

import java.util.Random;

public class HomeSimulation {
    public static void main(String[] args) {

        Random sim = new Random();
        int steps = 15;
        int event;
        while (steps > 0) {
            event = sim.nextInt(60);
            Controller.getController().control(event);
            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            steps--;
        }


    }
}
