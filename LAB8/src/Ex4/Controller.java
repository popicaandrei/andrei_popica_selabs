package Ex4;

import java.util.ArrayList;
import java.util.Random;

public class Controller {

    private static Controller ContUnit = null;

    private Temperature TemperatureSensor;
    private ArrayList<Fire> fireList = new ArrayList<Fire>();
    private Random r = new Random();

    private Controller() {
        TemperatureSensor = new Temperature(11, new Cooling(), new Heating());
        Fire f1 = new Fire(0, new GsmUnit(), new Alarm());
        Fire f2 = new Fire(1, new GsmUnit(), new Alarm());
        Fire f3 = new Fire(2, new GsmUnit(), new Alarm());
        Fire f4 = new Fire(3, new GsmUnit(), new Alarm());
        fireList.add(f1);
        fireList.add(f2);
        fireList.add(f3);
        fireList.add(f4);
    }

    public static Controller getController() {
        if (ContUnit != null) return ContUnit;
        else return new Controller();
    }

    public void control(int event) {
        if (event < 20) {
            TemperatureSensor.temperature = r.nextInt(60);
            System.out.println("The temperature is: " + TemperatureSensor.temperature);
            if (TemperatureSensor.temperature < 25) {
                TemperatureSensor.getHeats().tooCold();
            }
            if (TemperatureSensor.temperature > 40) {
                TemperatureSensor.getCooling().tooHot();
            }
            if (TemperatureSensor.temperature >= 25 && TemperatureSensor.temperature <= 40) {
                System.out.println("The temperature is optimal!");
            }

        }
        if (event >= 20 && event <= 40) {
            int location = r.nextInt(3);
            fireList.get(location).SetSmoke(r.nextBoolean());
            if (fireList.get(location).GetSmoke() == true) {
                System.out.println("Smoke detected from: " + fireList.get(location).GetId());
                fireList.get(location).GetAlarms().alarmFire();
                fireList.get(location).GetGsm().callOwner();
            } else {
                System.out.println("no smoke!");
            }
        }
        if (event > 40) {
            System.out.println("everything fine");
        }
    }
}
