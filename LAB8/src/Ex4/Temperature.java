package Ex4;

public class Temperature {

        private Cooling cools;
        private Heating heats;
        public int temperature;
        private int id;


        public Temperature(int Id, Cooling c, Heating h) {
            this.id = Id;
            this.cools = c;
            this.heats = h;
        }

        public Cooling getCooling() {
            return this.cools;
        }

        public Heating getHeats() {
            return this.heats;
        }
}
