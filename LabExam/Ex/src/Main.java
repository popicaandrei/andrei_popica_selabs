import javax.swing.*;
import java.io.FileWriter;
import java.io.IOException;

public class Main extends JFrame {
    private JButton button;
    private JTextField textField;
    private final String  FILE_NAME ="output.txt";

    public Main() {

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(500, 500);
        setVisible(true);

        button = new JButton("Write!");
        textField = new JTextField();
        button.setVisible(true);
        textField.setVisible(true);
        button.setBounds(180, 200, 150, 50);
        textField.setBounds(180, 70, 150, 50);


        button.addActionListener(
                e -> {
                    String text = textField.getText();
                    try {
                        FileWriter wr = new FileWriter("src/" + FILE_NAME);
                        wr.write(text);
                        textField.setText("");
                        wr.close();
                    } catch (IOException error) {
                        error.printStackTrace();
                    } finally {
                        System.out.println("Operation done !");
                    }
                }
        );
        setLayout(null);
        add(button);
        add(textField);

    }

    public static void main(String[] args) {
        new Main();
    }

}