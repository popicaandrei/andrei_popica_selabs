package Ex4;

import javax.swing.*;

public class Window extends JFrame {

    public Window() {
        setSize(Utils.winSize, Utils.winSize);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}