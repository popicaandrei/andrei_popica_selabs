package Ex3;

import java.util.concurrent.Semaphore;

public class CounterThreads {
    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(1);
        Counter c1 = new Counter(0, 100, semaphore);
        Counter c2 = new Counter(100, 200, semaphore);

        c1.start();
        Thread.sleep(50);
        c2.start();
        Thread.sleep(50);

    }
}

