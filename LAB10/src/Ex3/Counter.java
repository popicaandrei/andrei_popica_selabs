package Ex3;

import java.util.concurrent.Semaphore;

class Counter extends Thread {
    private int start;
    private int end;
    private Semaphore semaphore;

    public Counter(int start, int end, Semaphore semaphore) {
        this.start = start;
        this.end = end;
        this.semaphore = semaphore;
    }

    public void run() {

        try {
            semaphore.acquire();
            for (int i = start; i < end; ++i) {
                System.out.println(i);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
