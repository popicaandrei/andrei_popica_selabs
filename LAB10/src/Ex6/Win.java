package Ex6;

import javax.swing.*;

public class Win extends JFrame {
    private Object monitor;
    private JTextField textField;

    Win(Object monitor) {

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(300, 200);
        setLayout(null);

        textField = new JTextField();
        textField.setBounds(10, 10, 280, 20);
        this.monitor = monitor;
        JButton startStop = new JButton("on/off");
        startStop.setBounds(10, 40, 280, 20);
        startStop.addActionListener(e -> {
            synchronized (monitor) {
                monitor.notify();
            }
        });
        add(textField);
        add(startStop);
        setVisible(true);
    }

    public void updateTimer(long val) {
        textField.setText("" + val);
    }
}


