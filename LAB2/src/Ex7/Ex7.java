package Ex7;
import java.util.Random;
import java.util.Scanner;

public class Ex7 {

    public static void main(String[] args) {
        Random r=new Random();
        Scanner in=new Scanner(System.in);

        int n=r.nextInt(5);
        int lives=3;
        int x=0;
        while(lives!=0 || lives<0)
        {
            System.out.println("Guess the number: ");
            x=in.nextInt();
            if(x>n)
            {
                lives--;
                System.out.println("Wrong! Answer too high! Remaining tries: " + lives);

            }
            if(x<n) {
                lives--;
                System.out.println("Wrong! Answer too low! Remaining tries: " + lives);
            }
            if(x==n){


                System.out.println("Correct!");
                continue;}
            if(lives==0) {
                System.out.println("You lost! The number is: " + n);
                break;
            }
        }
    }
}
