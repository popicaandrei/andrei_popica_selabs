package Ex6;
import java.util.Scanner;

public class Ex6 {
    int factorial1(int n)
    {
        int fact=1;
        for(int i=1; i<=n; i++)
        {
            fact=fact*i;
        }
        return fact;

    }
    int factorial2(int n)
    {
        if(n==1)
            return 1;
        else
            return(n*factorial2(n-1));
    }

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        Ex6 object=new Ex6();

        System.out.println(object.factorial1(n));
        System.out.println(" "+object.factorial2(n));
    }
}
