package Ex5;
import java.util.Scanner;

public class Ex5 {
    void bubbleSort(int arr[])
    {
        int n=arr.length;
        for(int i=0;i<n-1; i++)
            for(int j=0; j<n-i-1; j++)
            {
                if(arr[j]>arr[j+1])
                {
                    int temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                }
            }
    }
    void printArray(int arr[])
    {
        int n=arr.length;
        for(int i=0; i<n; i++)
        {
            System.out.print(arr[i]+ " ");
        }
    }

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int[] array=new int [10];
        Ex5 object=new Ex5();
        System.out.println("Enter 10 elements for the array: ");
        for(int i=0; i<10; i++)
        {
            array[i]=in.nextInt();
        }
        object.printArray(array);
        System.out.println(" ");
        object.bubbleSort(array);
        object.printArray(array);
    }
}
