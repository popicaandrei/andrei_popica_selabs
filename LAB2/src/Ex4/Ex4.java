package Ex4;

import java.util.Scanner;
public class Ex4 {


        void sieveOfEratosthenes(int A, int B)
        {

            int count=0;
            int n=B-A;
            boolean prime[] = new boolean[n+1];
            for(int i=A;i<n;i++)
                prime[i] = true;

            for(int p = 2; p*p <=n; p++)
            {

                if(prime[p] == true)
                {

                    for(int i = p*2; i <= n; i += p)
                        prime[i] = false;
                }
            }


            for(int i = A; i <= n; i++)
            {
                if(prime[i] == true) {
                    count+=1;
                    System.out.print(i + " ");
                }
            }
            System.out.println(" ");
            System.out.println(count);
        }
    public static void main(String[] args) {
    Scanner in=new Scanner(System.in);


        int A=0;
        int B=0;
        System.out.println("Enter the starting and the ending point: ");
        A=in.nextInt();
        B=in.nextInt();
        Ex4 g=new Ex4();
        g.sieveOfEratosthenes(A, B);





    }
}
