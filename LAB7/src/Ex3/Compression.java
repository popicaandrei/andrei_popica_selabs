package Ex3;

import java.io.*;
import java.util.Scanner;


public class Compression {

    static void encryptFile(String name) {
        try {


            String newname = "";
            int i = 0;
            while (name.charAt(i) != '.') {
                newname = newname + name.charAt(i);
                i++;
            }
            newname = newname + ".enc";

            File file = new File("/Users/radu/IdeaProjects/work/Lab07/src/Lab07_ex03/" + newname);

            BufferedReader input = new BufferedReader(new FileReader(name));
            BufferedWriter output = new BufferedWriter(new FileWriter(newname));
            int character;
            while ((character = input.read()) != -1) {
                character = character << 1;
                output.write(character);
            }

            input.close();
            output.close();
        } catch (IOException e) {
            System.err.println("I/O error: " + e.getMessage());
        }
    }

    static void decryptFile(String name) {

        try {
            String newname = "";
            int i = 0;
            while (name.charAt(i) != '.') {
                newname = newname + name.charAt(i);
                i++;
            }
            newname = newname + ".dec";

            File file = new File("/Users/radu/IdeaProjects/work/Lab07/src/Lab07_ex03/" + newname);

            BufferedWriter output = new BufferedWriter(new FileWriter(newname));
            BufferedReader input = new BufferedReader(new FileReader(name));
            int character;
            while ((character = input.read()) != -1) {
                character = character >> 1;
                output.write((char) character);
            }

            input.close();
            output.close();

        } catch (IOException e) {
            System.err.println("I/O error: " + e.getMessage());
        }
    }


    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        int opt = 1;
        while (opt != 0) {
            System.out.println("1.Encrypt ");
            System.out.println("2.Decrypt ");
            System.out.println("0.Close");
            System.out.println("Option is:");
            opt = in.nextInt();
            switch (opt) {
                case 1: {
                    System.out.print("\r\nGive the file to encrypt:");
                    String file = in.next();
                    encryptFile("C:\\Users\\Andrei Popica\\andrei_popica_selabs\\LAB7\\src\\Ex3\\" + file);
                    break;
                }
                case 2: {
                    System.out.print("\r\nGive the file to decrypt:");
                    String file = in.next();
                    decryptFile("C:\\Users\\Andrei Popica\\andrei_popica_selabs\\LAB7\\src\\Ex3\\text.enc" + file);
                    break;
                }
                case 0: {
                    break;
                }
                default: {
                    System.out.println("Invalid command!");
                }
            }

        }

    }

}