package Ex4;

import java.io.Serializable;

public class Car implements Serializable{

    private String model;
    private double price;
    public Car(String mod , double pri){this.model=mod;this.price=pri;}
    public Car(){}

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}