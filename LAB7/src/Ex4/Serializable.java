package Ex4;

import java.io.*;
import java.util.Scanner;

public class Serializable {

    public static void SaveCar(Car masina) throws IOException {
        try {
            FileOutputStream fout = new FileOutputStream("src/Ex4/Ford.ser"+masina.getModel()+".ser");
            ObjectOutputStream stdout = new ObjectOutputStream(fout);
            stdout.writeObject(masina);
            System.out.println("Object saved!");
            stdout.close();
            fout.close();
        } catch (IOException e) {
            System.out.println("Error");
        }
    }


    public static void ReadCar(String obj) {

        Car masina = new Car();
        try {
            FileInputStream in = new FileInputStream("src/Ex4/" + obj+".ser");
            ObjectInputStream stdin = new ObjectInputStream(in);
            masina = (Car) stdin.readObject();

        } catch (IOException e) {
            System.out.println("Object not found!\r\n");
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("The class of the object not found (in active project)!\r\n");
            return;
        }

        System.out.println("The Car read :");
        System.out.print("Model: " + masina.getModel() + "  Price: " + masina.getPrice());

    }

    public static void ViewCars(){
        File f = new File("src/Ex4/");
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File FILE,String name) {
                return name.endsWith("ser");
            }
        };
        File[] files = f.listFiles(filter);
        System.out.println("The current serialized objects are:");
        for (File file : files) {
            System.out.println(file.getName());
        }

    }

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        int opt = 1;
        while (opt != 0) {
            System.out.println("\r\n\r\n1.Create & Save car");
            System.out.println("2.Read car");
            System.out.println("3.See the cars saved");
            System.out.println("0.Close");
            System.out.println("Give command:");
            opt = in.nextInt();
            switch (opt) {
                case 1:{
                    double pret;String model;
                    System.out.print("Give model :");model=in.next();
                    System.out.print("Give price :");pret=in.nextDouble();
                    SaveCar(new Car(model,pret));
                    break;
                }
                case 2:{
                    String object;
                    System.out.print("Give the model you want :");object=in.next();
                    ReadCar(object);
                    break;
                }
                case 3:{
                    ViewCars();
                    break;
                }
                case 0 :{break;}
                default:{System.out.print("Invalid Operation!");break;}
            }
        }
    }
}